## grammle.codeberg.page

[![Screenshot Landing-Page](https://codeberg.org/Grammle/Grammle/raw/branch/master/assets/22-10-25%20Screenshot%20Landingpage.png)](https://grammle.codeberg.page)

Dieses Repository beinhaltet die Grammle-Website, deren Quellcode mit Hugo generiert wurde und im Repo [Grammle/Grammle](https://codeberg.org/Grammle/Grammle) zu finden ist. Dort findest du auch mehr Infos zur Lizenz.